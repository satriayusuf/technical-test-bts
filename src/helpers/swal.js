import Swal from 'sweetalert2/dist/sweetalert2.js'
const state = {
  options: {
    customClass: {
      popup: '!px-3',
      title: '!text-zinc-600',
      htmlContainer: '!text-zinc-500',
      actions: 'gap-2 !mt-10 ',
      confirmButton: 'btn btn-primary',
      denyButton: 'btn btn-ghost text-error',
      cancelButton: 'btn btn-ghost'
    },
    buttonsStyling: false
  }
}

class TokenHelper {
  fire (options) {
    const swalOptions = { ...state.options, ...options }
    return Swal.fire(swalOptions)
  }

  toast ({ title, icon, timer }, otherOptions) {
    const options = {
      icon,
      title,
      toast: true,
      timer: timer || 1500,
      showConfirmButton: false,
      timerProgressBar: true,
      position: 'bottom',
      ...otherOptions
    }
    const swalOptions = { ...state.options, ...options }

    return Swal.fire(swalOptions)
  }

  alert ({ icon, title, html, text, confirmButtonText, cancelButtonText }, otherOptions) {
    const options = {
      icon,
      title,
      text,
      html,
      showConfirmButton: !!confirmButtonText,
      showCancelButton: !!cancelButtonText,
      confirmButtonText,
      cancelButtonText
    }
    const swalOptions = { ...state.options, ...options, ...otherOptions }

    if (swalOptions.showConfirmButton || swalOptions.showCancelButton) {
      if (swalOptions.showCancelButton) {
        swalOptions.customClass.confirmButton = 'btn btn-ghost border-base-300'
        swalOptions.customClass.cancelButton = 'btn btn-ghost'
      } else {
        swalOptions.customClass.confirmButton = 'btn btn-ghost'
      }
    } else {
      swalOptions.timer = 1500
      swalOptions.timerProgressBar = true
    }

    return Swal.fire(swalOptions)
  }

  confirm ({ title, text, confirmButtonText, cancelButtonText }, otherOptions) {
    const options = {
      icon: 'question',
      title,
      text,
      cancelButtonText: cancelButtonText || 'Tidak',
      confirmButtonText: confirmButtonText || 'Ya',
      showCancelButton: true
    }

    const swalOptions = { ...state.options, ...options, ...otherOptions }
    return Swal.fire(swalOptions)
  }

  confirmDelete ({ title, text, confirmButtonText, cancelButtonText }, otherOptions) {
    const options = {
      icon: 'question',
      title,
      text,
      confirmButtonText: confirmButtonText || 'Ya, Hapus',
      cancelButtonText: cancelButtonText || 'Batal',
      showCancelButton: true
    }

    const swalOptions = { ...state.options, ...options, ...otherOptions }
    swalOptions.customClass.confirmButton = 'btn btn-error'

    return Swal.fire(swalOptions)
  }
}

export default new TokenHelper()
