class TokenHelper {
  setAuth ({ token }) {
    const auth = JSON.stringify({ token })
    localStorage.setItem('_auth', auth)
  }

  getAuth () {
    const auth = JSON.parse(localStorage.getItem('_auth') || '{}')
    return auth
  }

  setToken ({ token }) {
    const auth = JSON.parse(localStorage.getItem('_auth') || '{}')
    auth.token = token
    localStorage.setItem('_auth', JSON.stringify(auth))
  }

  removeAuth () {
    localStorage.removeItem('_auth')
  }
}

export default new TokenHelper()
