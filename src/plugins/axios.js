import axios from 'axios'
import VueAxios from 'vue-axios'
import TokenHelper from '@/helpers/token'

let Axios = document.axios

if (!document.axios) {
    document.requestTokenState = 'done'
    document.axios = axios.create();
    document.axios.defaults.baseURL = import.meta.env.VITE_AXIOS_BASE_URL
    document.axios.defaults.headers['Content-Type'] = 'application/json'

    document.axios.interceptors.request.use(
    (config) => {
      const { token } = TokenHelper.getAuth()
      if (token) {
        config.headers.Authorization = 'Bearer ' + token
      }
      return config
    },
    (error) => {
      return Promise.reject(error)
    })
}

Axios = document.axios

export { Axios, VueAxios }
