import VueSweetalert2 from 'vue-sweetalert2'

const options = {
  confirmButtonColor: '#584910'
}

export { VueSweetalert2, options }
