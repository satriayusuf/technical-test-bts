const routes = [
  {
    path: '/login',
    component: () => import('@/views/guest/FormLogin.vue')
  },
  {
    path: '/register',
    component: () => import('@/views/guest/FormRegister.vue')
  }
]
export default routes
