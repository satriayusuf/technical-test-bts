import { useAuthStore } from '@/stores/auth'
import TokenHelper from '@/helpers/token'

const middleware = async (to, from, next) => {
    const auth = useAuthStore()
    const localAuth = TokenHelper.getAuth()
    const hasLocalAuth = Object.keys(localAuth).length > 0

    const needAuth = to.meta.requireAuth
    const needAdmin = to.meta.isAdmin

    const nextPage = (route) => {
        next(route)
    }

    if ((!auth.hasLoggedIn || !hasLocalAuth) && needAuth) {
        return nextPage('/login')
    } else if ((auth.hasLoggedIn && hasLocalAuth) && (to.path === '/login' || to.path === '/register')) {
            nextPage('/user/dashboard')
    } else {
        if ((auth.hasLoggedIn && hasLocalAuth) && (needAuth || needAdmin)) {
                nextPage()
          } else {
            nextPage()
          }
    }
}

export default middleware