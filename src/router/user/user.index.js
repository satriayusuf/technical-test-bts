const routes = [
  
    {
      path: '/user',
      name: 'user',
      component: () => import('@/views/user/UserPage.vue'),
      meta: {
        requireAuth: true
      },
      children : [
        {
          path: 'dashboard',
          name:'dashboard',
          component: () => import('@/views/user/DashboardPage.vue'),
        }
      ]
    }
  ]
  export default routes
  