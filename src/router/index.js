import { createRouter, createWebHistory } from 'vue-router'
import routeAuth from '@/router/auth/auth.index.js'
import routeUser from '@/router/user/user.index.js'
import middleware from '@/router/middleware'


const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/guest/LandingPage.vue')
    },
    ...routeAuth,
    ...routeUser
  ]
})

router.beforeEach((to, from, next) => middleware(to, from, next))

export default router
