import { defineStore } from 'pinia'
import token from '@/helpers/token'

const auth = token.getAuth()

const initialState = Object.keys(auth).length > 0
  ? {
      auth: auth,
      hasLoggedIn: true,
    }
  : {
      auth: {
        token: null,
      },
      hasLoggedIn: false,
      downgrade: null
    }

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => initialState,
  actions: {
    setToken ({
      token
    }) {
      this.auth.token = token
      this.hasLoggedIn = true
    },
    setAuth ({
      token,
    }) {
      this.auth = {
        token
      }

      this.hasLoggedIn = true
    },
    unsetAuth () {
      this.auth = {
        token: null,
      }

      this.hasLoggedIn = false
    }
  }
})
