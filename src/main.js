import '@/assets/tailwind.css'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import {Axios} from '@/plugins/axios'
import { VueSweetalert2, options } from '@/plugins/swal'
import 'sweetalert2/dist/sweetalert2.min.css';


import App from './App.vue'
import router from './router'
  // register globally

const app = createApp(App)
app.use(VueSweetalert2, options)


const appInstance = app.use(createPinia())

appInstance.config.globalProperties.$axios = Axios

appInstance.use(router)

appInstance.mount('#app')

appInstance.provide('axios', appInstance.config.globalProperties.$axios)
