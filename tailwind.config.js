/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      {
        'multidesa-light': { /* your theme name */
          primary: '#4db7f0', /* Primary color */
          'primary-focus': '#473B0B', /* Primary color - focused */
          'primary-content': '#F9FAFB', /* Foreground content color to use on primary color */

          secondary: '#f39113', /* Secondary color */
          'secondary-focus': '#d1761c', /* Secondary color - focused */
          'secondary-content': '#F9FAFB', /* Foreground content color to use on secondary color */

          accent: '#DFD3BE', /* Accent color */
          'accent-focus': '#DFD3BE', /* Accent color - focused */
          'accent-content': '#DFD3BE', /* Foreground content color to use on accent color */

          neutral: '#686868', /* Neutral color */
          'neutral-focus': '#A72828', /* Neutral color - focused */
          'neutral-content': '#ffffff', /* Foreground content color to use on neutral color */

          'base-100': '#ffffff', /* Base color of page, used for blank backgrounds */
          'base-200': '#f5f5f5', /* Base color, a little darker */
          'base-300': '#E1E1E1', /* Base color, even more darker */
          'base-content': '#686868', /* Foreground content color to use on base color */

          // info: '#58AFFC', /* Info */
          // 'info-content': '#0e5798',

          // success: '#27DD70', /* Success */
          // 'success-content': '#236827', /* Success */

          // warning: '#FFCD1C', /* Warning */
          // 'warning-content': '#c97000', /* Warning */

          // error: '#FF5456', /* Error */
          // 'error-content': '#FFE8DC' /* Error */
        }
      }], // true: all themes | false: only light + dark | array: specific themes like this ["light", "dark", "cupcake"]
    darkTheme: "light", // name of one of the included themes for dark mode
    base: false, // applies background color and foreground color for root element by default
    styled: true, // include daisyUI colors and design decisions for all components
    utils: true, // adds responsive and modifier utility classes
    rtl: false, // rotate style direction from left-to-right to right-to-left. You also need to add dir="rtl" to your html tag and install `tailwindcss-flip` plugin for Tailwind CSS.
    prefix: "", // prefix for daisyUI classnames (components, modifiers and responsive class names. Not colors)
    logs: true, // Shows info about daisyUI version and used config in the console when building your CSS
  },
  
}

